import { Injectable, Injector } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';
 
@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  
  constructor(
    private injector: Injector) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // add authorization header with jwt token if available
    
    const accessToken = localStorage.getItem('auth-token');
    if (accessToken) {
      request = request.clone({
        setHeaders: {
            'auth-token': `${accessToken}`
        }
    });
    }

    return next.handle(request).pipe(
      tap((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {        
          // do stuff with response if you want
        }
      }, (err: any) => {
        if (err instanceof HttpErrorResponse) {        
          if (err.status === 401) {
            this.injector.get(Router).navigate(['/login']);
          } else {
          }
        }
      })
    ) 
  }
}
