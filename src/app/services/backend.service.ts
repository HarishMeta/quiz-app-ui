import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class BackendService {
  contentHeaders: any;

  user: any;
  constructor(private http: HttpClient) {}

  getService(endpoint) {
    return this.http.get(endpoint);
  }

  getQuizInfo() {
    return this.http.get(`http://13.233.140.114:8080/api/v1/quiz`);
  }

  login(data) {
    return this.http.post(`http://13.233.140.114:8081/api/v1/auth/login`, data);
  }

  getQuestion(quizId: any, questionId: any) {
    return this.http.get(`http://13.233.140.114:8080/api/v1/quiz/${quizId}/questions/${questionId}`);
  }

  getAllQuestion(quizId: any) {
    return this.http.get(`http://13.233.140.114:8080/api/v1/quiz/${quizId}/questions`);
  }

  quizStatus(data) {
    return this.http.post(`http://13.233.140.114:8080/api/v1/user/quiz `, data, { headers: new HttpHeaders({'Accept': 'application/json', 'Content-Type': 'application/json'})});
  }

  submitAnswer(data) {
    return this.http.post(`http://13.233.140.114:8080/api/v1/quiz/questions/answer`, data, { headers: new HttpHeaders({'Accept': 'application/json', 'Content-Type': 'application/json'})});
  }

  submitQuiz(data) {
    return this.http.post(`http://13.233.140.114:8080/api/v1/quiz/submit`, data, { headers: new HttpHeaders({'Accept': 'application/json', 'Content-Type': 'application/json'})});
  }
}