import { Injectable, OnInit } from '@angular/core';
import { CanActivate, RouterStateSnapshot, ActivatedRouteSnapshot, Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Observable, Observer } from 'rxjs';

@Injectable()
export class AuthGuard implements CanActivate {
  page: any;
  permissions: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) {}

  canActivate(): Observable<boolean> | Promise<boolean> | boolean {

    return new Observable((observer: Observer<boolean>) => {
      let token = localStorage.getItem('auth-token');
      if (!token) {            
        this.router.navigate(['/login']);
        observer.next(false);
        observer.complete();
      } else {
        observer.next(true);
        observer.complete();
      }
    });
  }
}
