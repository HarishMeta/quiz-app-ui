import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable, Observer } from 'rxjs';

@Injectable()
export class AuthLoginGuard implements CanActivate {

  constructor(
    private router: Router, 
  ) {}

  canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    return new Observable((observer: Observer<boolean>) => {
      let token = localStorage.getItem('auth-token');
      if (token) {
        this.router.navigate(['/']);
        observer.next(false);
        observer.complete();
      } else {
        observer.next(true);
        observer.complete();
      }
    });
  }
}
