import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { BackendService } from './services/backend.service';
import { JwtInterceptor } from './services/jwt.interceptor';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AppRoutes } from './app.routing';
import { RouterModule } from '@angular/router';
import { AuthGuard } from './services/auth.guard';
import { AuthLoginGuard } from './services/auth.login.guard';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
  ],
  imports: [
    HttpClientModule,
    RouterModule.forRoot(AppRoutes),
    FormsModule,
    CommonModule,
    BrowserModule
  ],
  providers: [
    BackendService, 
    AuthGuard,
    AuthLoginGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
