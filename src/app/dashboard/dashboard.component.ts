import { Component, OnInit } from '@angular/core';
import { BackendService } from '../services/backend.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {
  
  quiz: any;
  quizId = null;
  quizInfo: any = [];
  quizResult: any;
  currentQ: any = {
    choices: [],
    text: ''
  };
  currentA = null;

  activeQ = null;

  isStarted: boolean = false;
  isSubmitted = false;

  list = []

  constructor(
    public router: Router,
    public service: BackendService) {
    // this.getAllQuestions();
    this.getQuizInfo();
  }

  logout(e : Event) {
    e.preventDefault();
    localStorage.clear();
    this.router.navigateByUrl('/login'); 
  }

  getQuizInfo() {
    this.service.getQuizInfo()
      .subscribe((res: any) => {
        this.quiz = res;
        this.quizId = res.id;
        this.getAllQuestions();
      })
  }

  getAllQuestions() {
    this.service.getAllQuestion(this.quizId)
      .subscribe(res => {
        this.quizInfo = res;
      });
  }

  getQuestion(quizId: any, questionId: any) {
    this.service.getQuestion(quizId, questionId)
      .subscribe((res: any) => {
        this.currentQ = res;
        this.currentQ.ans = null;
      });
  }

  startQuiz(e: any) {
    e.preventDefault();
    this.isStarted = true;
    this.activeQ = 0;
    this.getQuestion(this.quizId, this.quizInfo[0].id);
    // this.service.quizStatus({ quizId: this.quizId, status: 'IN_PROGRESS'})
    //   .subscribe(res => {
    //     console.log(res);
    //   });
  }

  nextClick() {
    if (this.activeQ <= this.quizInfo.length) {
      if (this.quizInfo[this.activeQ].ans) {
        this.service.submitAnswer({choiceId: this.quizInfo[this.activeQ].ans, quizId :this.quizId, questionId: this.currentQ.id})
          .subscribe(res => {
            console.log(res);
          });
      }
      this.activeQ = this.activeQ + 1;
      this.getQuestion(this.quizId, this.quizInfo[this.activeQ].id);
    }
  }

  previousClick() {
    if (this.activeQ != 0) {
      this.activeQ = this.activeQ - 1;
      this.getQuestion(this.quizId, this.quizInfo[this.activeQ].id);
    }
  }

  submitClick() {
    // this.service.quizStatus({ userId: 1, quizId: this.quizId, status: 'COMPLETED'})
    // .subscribe(res => {
    //   console.log(res);
    // });

if (this.quizInfo[this.activeQ].ans) {
      this.service.submitAnswer({choiceId: this.quizInfo[this.activeQ].ans, quizId :this.quizId, questionId: this.currentQ.id})
          .subscribe(res => {

    this.service.submitQuiz({id :this.quizId})
        .subscribe(res => {
          this.quizResult = res;
          this.isSubmitted = true;
        });

            console.log(res);
          });
} else {
      this.service.submitQuiz({id :this.quizId})
        .subscribe(res => {
          this.quizResult = res;
          this.isSubmitted = true;
        });
   }
}}