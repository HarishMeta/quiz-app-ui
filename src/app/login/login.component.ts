import { Component, OnInit } from '@angular/core';
import { BackendService } from '../services/backend.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form = {
    email: null,
    password: null
  };

  constructor(
    private router: Router,
    public service: BackendService) { }

  ngOnInit() {
  }

  login() {
    this.service.login(this.form)
      .subscribe((res: any) => {
        this.service.user = res;
        localStorage.setItem('auth-token', res.authToken);
        this.router.navigateByUrl('/dashboard');
      });
  }

}
