import { Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthGuard } from './services/auth.guard';
import { AuthLoginGuard } from './services/auth.login.guard';

export const AppRoutes: Routes = [{
  path: '',
  redirectTo: '/dashboard',
  pathMatch: 'full'
},
{
  path: 'dashboard',
  canActivate: [AuthGuard],
  component: DashboardComponent
},
{
  path: 'login',
  canActivate: [AuthLoginGuard],
  component: LoginComponent,
}, {
  path: '**',
  redirectTo: 'login'
}];